import { Node, Location } from './node.model';
import { SyntaxType } from './type.constant';
import { Expression, Identifier } from './expression.model';

export interface StatementListItem{}

export class Statement extends Node implements StatementListItem{

    constructor(type: string, location: Location) {
        super(type, location);
    }

}

export class BlockStatement extends Statement{
    readonly statements: Statement[];

    constructor(location: Location, statements: Statement[]) {
        super(SyntaxType.BlockStatement, location);
        this.statements = statements;
    }
}

export class IfStatement extends Statement {
    readonly condition: Expression;
    readonly consequent: Statement; //if condition returns true
    readonly alternative: Statement | null; //if condition returns false

    constructor(location: Location,
        condition: Expression,
        consequent: Statement,
        alternative: Statement | null
    ) {
        super(SyntaxType.IfStatement, location);
        this.condition = condition;
        this.consequent = consequent;
        this.alternative = alternative;
    }
}

export class BreakStatement extends Statement {
    readonly label: Identifier | null;
    constructor(location: Location, label: Identifier | null){
        super(SyntaxType.BreakStatement, location);
        this.label = label;
    }
}

export class ContinueStatement extends Statement {
    readonly label: Identifier | null;
    constructor(location: Location, label: Identifier | null) {
        super(SyntaxType.ContinueStatement, location);
        this.label = label;
    }
}

export class DebuggerStatement extends Statement {
    constructor(location: Location) {
        super(SyntaxType.DebuggerStatement, location);
    }
}

export class DoWhileStatement extends Statement {
    readonly body: Statement;
    readonly test: Expression;
    constructor(location: Location, body: Statement, test: Expression) {
        super(SyntaxType.DoWhileStatement, location);
        this.body = body;
        this.test = test;
    }
}

export class EmptyStatement extends Statement{
    constructor(location: Location) {
        super(SyntaxType.EmptyStatement, location);
    }
}

export class ExpressionStatement extends Statement {
    readonly expression: Expression;
    constructor(location: Location, expression: Expression) {
        super(SyntaxType.ExpressionStatement, location);
        this.expression = expression;
    }
}

export class ForStatement extends Statement {
    readonly init: Expression;
    readonly test: Expression;
    readonly update: Expression;
    readonly loopBody: Statement;
    constructor(location: Location, init: Expression, test: Expression, update: Expression, loopBody: Expression) {
        super(SyntaxType.ForStatement, location);
        this.init = init;
        this.test = test;
        this.update = update;
        this.loopBody = loopBody;
    }
}

export class ForInStatement extends Statement {
    readonly left: Expression;
    readonly right: Expression;
    readonly loopBody: Statement;
    constructor(location: Location, left: Expression, right: Expression, loopBody: Expression) {
        super(SyntaxType.ForInStatement, location);
        this.left = left;
        this.right = right;
        this.loopBody = loopBody;
    }
}

export class ForOfStatement extends Statement {
    readonly left: Expression;
    readonly right: Expression;
    readonly loopBody: Statement;
    constructor(location: Location, left: Expression, right: Expression, loopBody: Expression) {
        super(SyntaxType.ForOfStatement, location);
        this.left = left;
        this.right = right;
        this.loopBody = loopBody;
    }
}

export class LabelledStatement extends Statement{
    readonly label: Identifier;
    readonly body: Statement;
    constructor(location: Location, label: Identifier, body: Statement) {
        super(SyntaxType.LabelledStatement, location);
        this.label = label;
        this.body = body;
    }
}

export class ReturnStatement extends Statement{
    readonly argument: Expression | null;
    constructor(location: Location, argument: Expression | null) {
        super(SyntaxType.ReturnStatement, location);
        this.argument = argument
    }
}

export class SwitchStatement extends Statement {
    readonly switchTest: Expression;
    readonly cases: SwitchCase[];
    constructor(location: Location, switchTest: Expression, cases: SwitchCase[]) {
        super(SyntaxType.SwitchStatement, location);
        this.switchTest = switchTest;
        this.cases = cases;
    }
}

export class SwitchCase extends Node{
    readonly caseTest: Expression | null;
    readonly consequent: Statement[];
    constructor(location: Location, caseTest: Expression, consequent: Statement[]) {
        super(SyntaxType.SwitchCase, location);
        this.caseTest = caseTest;
        this.consequent = consequent;
    }
}

export class ThrowStatement extends Statement {
    readonly argument: Expression;
    constructor(location: Location, argument: Expression) {
        super(SyntaxType.ThrowStatement, location);
        this.argument = argument;
    }
}

export class TryStatement extends Statement {
    readonly tryBlock: BlockStatement;
    readonly handler: CatchClause | null;
    readonly finalBlock: BlockStatement;
    constructor(location: Location, tryBlock: BlockStatement, handler: CatchClause | null, finalBlock: BlockStatement) {
        super(SyntaxType.TryStatement, location);
        this.tryBlock = tryBlock;
        this.handler = handler;
        this.finalBlock = finalBlock;
    }
}

export class CatchClause extends Node {
    readonly param: Identifier;
    readonly body: BlockStatement
    constructor(location: Location, param: Identifier, body: BlockStatement) {
        super(SyntaxType.CatchClause, location);
        this.param = param;
        this.body = body;
    }
}

export class WhileStatement extends Statement {
    readonly test: Expression;
    readonly body: Statement;
    constructor(location: Location, test: Expression, body: Statement) {
        super(SyntaxType.WhileStatement, location);
        this.test = test;
        this.body = body;
    }
}

export class WithStatement extends Statement {
    readonly paramObject: Expression;
    readonly body: Statement;
    constructor(location: Location, paramObject: Expression, body: Statement) {
        super(SyntaxType.WithStatement, location);
        this.paramObject = paramObject;
        this.body = body;
    }
}