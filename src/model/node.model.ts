import { SyntaxType } from "./type.constant";
import { StatementListItem } from './statement.model';

// export type ArgumentListElement = Expression | SpreadElement;
// export type ArrayExpressionElement = Expression | SpreadElement | null;
// export type ArrayPatternElement = AssignmentPattern | BindingIdentifier | BindingPattern | RestElement | null;
// export type BindingPattern = ArrayPattern | ObjectPattern;
// export type BindingIdentifier = Identifier;
// export type Declaration = AsyncFunctionDeclaration | ClassDeclaration | ExportDeclaration | FunctionDeclaration | ImportDeclaration | VariableDeclaration;
// export type ExportableDefaultDeclaration = BindingIdentifier | BindingPattern | ClassDeclaration | Expression | FunctionDeclaration;
// export type ExportableNamedDeclaration = AsyncFunctionDeclaration | ClassDeclaration | FunctionDeclaration | VariableDeclaration;
// export type ExportDeclaration = ExportAllDeclaration | ExportDefaultDeclaration | ExportNamedDeclaration;
// export type Expression = ArrayExpression | ArrowFunctionExpression | AssignmentExpression | AsyncArrowFunctionExpression | AsyncFunctionExpression |
//     AwaitExpression | BinaryExpression | CallExpression | ClassExpression | ComputedMemberExpression |
//     ConditionalExpression | Identifier | FunctionExpression | Literal | NewExpression | ObjectExpression |
//     RegexLiteral | SequenceExpression | StaticMemberExpression | TaggedTemplateExpression |
//     ThisExpression | UnaryExpression | UpdateExpression | YieldExpression;
// export type FunctionParameter = AssignmentPattern | BindingIdentifier | BindingPattern;
// export type ImportDeclarationSpecifier = ImportDefaultSpecifier | ImportNamespaceSpecifier | ImportSpecifier;
// export type ObjectExpressionProperty = Property | SpreadElement;
// export type ObjectPatternProperty = Property | RestElement;
// export type Statement = AsyncFunctionDeclaration | BreakStatement | ContinueStatement | DebuggerStatement | DoWhileStatement |
//     EmptyStatement | ExpressionStatement | Directive | ForStatement | ForInStatement | ForOfStatement |
//     FunctionDeclaration | IfStatement | ReturnStatement | SwitchStatement | ThrowStatement |
//     TryStatement | VariableDeclaration | WhileStatement | WithStatement;
// export type PropertyKey = Identifier | Literal;
// export type PropertyValue = AssignmentPattern | AsyncFunctionExpression | BindingIdentifier | BindingPattern | FunctionExpression;
// export type StatementListItem = Declaration | Statement;

export class Location{
    readonly line: number;
    readonly col: number;
    readonly offset: number;
    constructor(line: number, col: number, offset: number) {
        this.line = line;
        this.col = col;
        this.offset = offset;
    }
}

export class Node {
    readonly type: string;
    readonly location: Location;
    constructor(type: string, location: Location) {
        this.type = type;
        this.location = location;
    }
}

export class Program extends Node{
    readonly body: StatementListItem[];
    constructor(location: Location, body: StatementListItem[]) {
        super(SyntaxType.Program, location);
        this.body = body;
    }
}

export class Super extends Node {
    constructor(location: Location) {
        super(SyntaxType.Super, location);
    }
}