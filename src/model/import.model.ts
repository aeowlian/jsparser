import { Node, Location } from './node.model';
import { SyntaxType } from './type.constant';

export class Import extends Node{
    constructor(location: Location) {
        super(SyntaxType.Import, location);
    }
}