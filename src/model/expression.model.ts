import { Node, Location } from './node.model';
import { SyntaxType } from './type.constant';
import { BlockStatement } from "./statement.model";
import { ObjectExpressionProperty } from './property.model';

export interface ArgumentListElement{}
export interface ArrayExpressionElement{}

export class Expression extends Node implements ArrayExpressionElement, ArgumentListElement{

    constructor(type: string, location: Location) {
        super(type, location);
    }

}

export class Identifier extends Expression {
    readonly name: string;
    constructor(location: Location, name: string) {
        super(SyntaxType.Identifier, location);
        this.name = name;
    }
}

export class ConditionalExpression extends Expression{
    readonly condition: Expression;
    readonly consequent: Expression; //if condition returns true
    readonly alternative: Expression; //if condition returns false

    constructor(location: Location,
        condition: Expression,
        consequent: Expression,
        alternative: Expression
    ) {
        super(SyntaxType.ConditionalExpression, location);
        this.condition = condition;
        this.consequent = consequent;
        this.alternative = alternative;
    }
}

export class Literal extends Expression{
    readonly value: boolean | number | string | null;
    readonly raw: string;

    constructor(location: Location, value: boolean | number | string | null, raw: string) {
        super(SyntaxType.Literal, location);
        this.value = value;
        this.raw = raw;
    }
}

export class BinaryExpression extends Expression {
    readonly operator: string;
    readonly left: Expression;
    readonly right: Expression;
    constructor(location: Location, operator: string, left: Expression, right: Expression) {
        const logical = (operator === '||' || operator === '&&');
        super(logical ? SyntaxType.LogicalExpression : SyntaxType.BinaryExpression, location);
        this.operator = operator;
        this.left = left;
        this.right = right;
    }
}

export class ArrayExpression extends Expression {
    readonly elements: ArrayExpressionElement[];
    constructor(location: Location, elements: ArrayExpressionElement[]) {
        super(SyntaxType.ArrayExpression, location);
        this.elements = elements;
    }
}

export class LambdaExpression extends Expression{
    readonly id: Identifier | null;
    // readonly params: FunctionParameter[]; //TODO
    readonly body: BlockStatement | Expression;
    readonly generator: boolean;
    readonly expression: boolean;
    readonly async: boolean;
    constructor(location: Location, /*params: FunctionParameter[],*/ body: BlockStatement | Expression, expression: boolean) {
        super(SyntaxType.LambdaExpression, location);
        this.id = null;
        // this.params = params;
        this.body = body;
        this.generator = false;
        this.expression = expression;
        this.async = false;
    }
}

export class AssignmentExpression extends Expression{
    readonly operator: string;
    readonly left: Expression;
    readonly right: Expression;
    constructor(location: Location, operator:string, left: Expression, right: Expression) {
        super(SyntaxType.AssignmentExpression, location);
        this.operator = operator;
        this.left = left;
        this.right = right;
    }
}

export class CallExpression extends Expression{
    readonly callee: Expression;
    readonly arguments: ArgumentListElement[];
    constructor(location: Location, callee: Expression, args: ArgumentListElement[]) {
        super(SyntaxType.CallExpression, location);
        this.callee = callee;
        this.arguments = args
    }
}

export class FunctionExpression extends Expression {
    readonly id: Identifier | null;
    // readonly params: FunctionParameter[]; //TODO
    readonly body: BlockStatement | Expression;
    readonly generator: boolean;
    readonly expression: boolean;
    readonly async: boolean;
    constructor(location: Location, /*params: FunctionParameter[],*/ body: BlockStatement | Expression, expression: boolean) {
        super(SyntaxType.FunctionExpression, location);
        this.id = null;
        // this.params = params;
        this.body = body;
        this.generator = false;
        this.expression = expression;
        this.async = false;
    }
}

export class NewExpression extends Expression {
    readonly objectClass: Expression;
    readonly arguments: ArgumentListElement[];
    constructor(location: Location, objectClass: Expression, args: ArgumentListElement[]) {
        super(SyntaxType.NewExpression, location);
        this.objectClass = objectClass;
        this.arguments = args;
    }
}

export class ObjectExpression extends Expression{
    readonly properties: ObjectExpressionProperty[];
    constructor(location: Location, properties: ObjectExpressionProperty[]) {
        super(SyntaxType.NewExpression, location);
        this.properties = properties;
    }
}

export class RegexLiteral extends Expression {
    readonly value: RegExp;
    readonly raw: string;
    readonly regex: {pattern: string, flags: string}
    constructor(location: Location, value: RegExp, raw: string, pattern: string, flags: string) {
        super(SyntaxType.NewExpression, location);
        this.value = value;
        this.raw = raw;
        this.regex = {pattern, flags};
    }
}

export class SequenceExpression extends Expression {
    readonly expressions: Expression[];
    constructor(location: Location, expressions: Expression[]) {
        super(SyntaxType.SequenceExpression, location);
        this.expressions = expressions;
    }
}

export class UnaryExpression extends Expression {
    readonly operator: string;
    readonly argument: Expression;
    readonly prefix: boolean;
    readonly update: boolean;
    constructor(location: Location, operator: string, argument: Expression, prefix: boolean, update: boolean) {
        super(SyntaxType.UnaryExpression, location);
        this.operator = operator;
        this.argument = argument;
        this.prefix = prefix;
        this.update = update;
    }
}

export class YieldExpression extends Expression {
    readonly argument: Expression | null;
    readonly isDelegate: boolean;
    constructor(location: Location, argument: Expression | null, isDelegate: boolean) {
        super(SyntaxType.YieldExpression, location);
        this.argument = argument;
        this.isDelegate = isDelegate;
    }
}

export class ThisExpression extends Expression {
    constructor(location: Location) {
        super(SyntaxType.ThisExpression, location);
    }
}