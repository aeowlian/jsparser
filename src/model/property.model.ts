import { Node, Location } from './node.model';
import { SyntaxType } from './type.constant';

export interface PropertyKey{};
export interface PropertyValue{};
export interface ObjectExpressionProperty{};
export interface ObjectPatternProperty{};

export class Property extends Node{
    readonly key: PropertyKey;
    readonly isComputed: boolean;
    readonly value: PropertyValue | null;
    readonly propertyKind: string;
    readonly isMethod: boolean;
    readonly isShorthand: boolean;
    constructor(location: Location, key: PropertyKey, value: PropertyValue, propertyKind: string, isMethod: boolean, isShorthand: boolean, isComputed: boolean) {
        super(SyntaxType.Property, location);
        this.key = key;
        this.value = value;
        this.propertyKind = propertyKind;
        this.isMethod = isMethod;
        this.isComputed = isComputed;
        this.isShorthand = isShorthand;
    }
}