const moo = require('moo');

export default class Tokenizer{
    tokenizer;
    
    constructor() {
        this.tokenizer = moo.compile({
            keyword: [
                'async',
                'break',
                'continue',
                'debugger',
                'do',
                'while',
                'for',
                'in',
                'of',
                'if',
                'else',
                'return',
                'switch',
                'case',
                'throw',
                'try',
                'catch',
                '=',
                'await',
                'try',
                'catch',
                'this',
                'new',
                'super',
                'with',
                'yield',
                
                //Declaration tokens
                'class',
                'import',
                'export',
                'default',
                'function',
                'let',
                'var',
                '=>',
                'static',
                'constructor'
                
                // 'while', 'if', 'else', 'const', 'default',
                // 'class', 'constructor', 'this', 'export', 'import'
            ],
            star: '*',
            lParen: '(',
            rParen: ')',
            lcBrace: '{',
            rcBrace: '}',
            lArray: '[',
            rArray: ']',
            comment: /\/\/.*?$/,
            number:  /0|[1-9][0-9]*/,
            string:  /['"`].*['"`]/,
            spread: /...[a-zA-Z$_][a-zA-Z$_0-9]*/,
            whitespace: { match: /\s\s*/, lineBreaks: true },
            end: ';',
            comma: ',',
            dot: '.',
            identifier: /[a-zA-Z_$][a-zA-Z0-9_$]*/,
            newline: { match: /\n/, lineBreaks: true },
            operator: ['+', '-', '*', '/', '==', '===', '<', '>', '<<', '>>', '<=', '>=', '!=', '^']
        });
    }

    tokenize(string) {
        console.log(string);
        this.tokenizer.reset(string);
        let temp;
        let result = [];
        temp = this.tokenizer.next();
        while(temp != null){
            result.push(temp);
            temp = this.tokenizer.next();
        }
        return result;
    }

    tokenizeFile(file, callback) {
        let fileReader = new FileReader();
        let tokenizerInstance = this.tokenizer;
        fileReader.onload = function (event) {
            let string = event.target.result;
            tokenizerInstance.reset(string);
            let temp;
            let result = [];
            temp = tokenizerInstance.next();
            while(temp != null){
                result.push(temp);
                temp = tokenizerInstance.next();
            }
            callback(result);
        }
        fileReader.readAsText(file);
    }
}